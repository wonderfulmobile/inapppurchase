//
//  MngMKStoreKit.m
//  AutoRenewable
//
//  Created by User on 8/11/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import "MngMKStoreKit.h"
#import "MKStoreKit.h"

@implementation MngMKStoreKit

- (void)checkIsPurchaseOrExpireDate {
    // To check if a product was previously purchased or not
    if ([[MKStoreKit sharedKit] isProductPurchased:@"ProductIdentifier"]) {
        // Unlock it
    }
    
    // To check fr a product's expiry date using-expiryDateForProduct as shown below
}

- (void)purchase {
    [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"ProductIdentifier"];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchasedNotification object:nil queue:[[NSOperationQueue alloc] init] usingBlock:^(NSNotification *note) {
        NSLog(@"Purchased/Subscribed to product with id: %@", [note object]);
        
        NSLog(@"%@", [[MKStoreKit sharedKit] valueForKey:@"purchaseRecord"]);
    }];
}

- (void) analysis:(NSMutableArray*)array {
    NSInteger maxDataMS = 0;
    for (NSDictionary* dictionary in array) {
        NSInteger expireDateMS = (NSInteger)[dictionary valueForKey:@"expires_date_ms"];
        
        if (expireDateMS > maxDataMS) {
            maxDataMS = expireDateMS;
        }
    }

    NSInteger currentDateMS = [[NSDate date] timeIntervalSince1970];
    
    if (currentDateMS > maxDataMS / 1000) {
        
    }
}

@end
