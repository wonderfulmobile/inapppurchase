//
//  ViewController.m
//  AutoRenewable
//
//  Created by User on 8/11/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import "ViewController.h"
#import "MKStoreKit.h"
#import <StoreKit/StoreKit.h>

@interface ViewController ()

@end

@implementation ViewController {
    NSNumberFormatter * _priceFormatter;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self loadProducts];
}

- (void) viewWillAppear:(BOOL)animated {
    //[self checkoutReceipt];
}

- (void) viewDidAppear:(BOOL)animated {
    
    
}

- (void) checkoutReceipt {
    NSURL* receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData* receipt = [NSData dataWithContentsOfURL:receiptURL];
    
    if (receipt == nil) {
        return;
    }
    
    // Create the JSON object that describes the request
    NSError *error;
    NSDictionary *requestContents = @{
                                      @"receipt-data": [receipt base64EncodedStringWithOptions:0]
                                      };
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) { // ... Handle error ... //
    }
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt"];     // buy
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                           completionHandler:^(NSURLResponse* response, NSData* data, NSError *connectionError) {
                               if (connectionError) {
                                   // ... Handle error ... //
                               } else {
                                   NSError *error;
                                   NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!jsonResponse) { // ... Handle error ...//
                                   }
                                   
                                   // ... Send a response back to the device ... //
                               }
                           }];
}

// 4
- (void)loadProducts {
    
    //[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    
    _products = nil;
    //[self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductsAvailableNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      _products = [[MKStoreKit sharedKit] availableProducts];
                                                      
                                                      NSLog(@"Products available: %@", [[MKStoreKit sharedKit] availableProducts]);
                                                      
                                                      for (SKProduct* pro in _products) {
                                                          NSString* str = pro.productIdentifier;
                                                          str = str;
                                                      }
                                                      
                                                      [self.tableView reloadData];
                                                      
                                                  }];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_products == nil) {
        return 0;
    }
    return _products.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.row >= _products.count) {
        cell.textLabel.text = @"Random Face";
        cell.detailTextLabel.text = @"Unlimited";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    SKProduct * product = (SKProduct *) _products[indexPath.row];
    cell.textLabel.text = product.localizedTitle;
    [_priceFormatter setLocale:product.priceLocale];
    cell.detailTextLabel.text = [_priceFormatter stringFromNumber:product.price];
    
//    if ([[MKStoreKit sharedKit] isProductPurchased:product.productIdentifier] ) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        cell.accessoryView = nil;
//    } else {
        UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buyButton.frame = CGRectMake(0, 0, 72, 37);
        [buyButton setTitle:@"Buy" forState:UIControlStateNormal];
        buyButton.tag = indexPath.row;
        [buyButton addTarget:self action:@selector(buyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.accessoryView = buyButton;
//    }
    
    return cell;
}

- (void)buyButtonTapped:(id)sender {
    
    UIButton *buyButton = (UIButton *)sender;
    SKProduct *product = _products[buyButton.tag];
    
    NSLog(@"Buying %@...", product.productIdentifier);
    [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:product.productIdentifier];
}

- (IBAction)restore:(id)sender {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
