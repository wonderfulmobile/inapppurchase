//
//  ViewController.h
//  AutoRenewable
//
//  Created by User on 8/11/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UITableViewController {
    NSArray* _products;
}

- (IBAction)restore:(id)sender;

@end

