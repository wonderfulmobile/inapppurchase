//
//  RageIAPHelper.m
//  InAppRage
//
//  Created by User on 7/28/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import "RageIAPHelper.h"

@implementation RageIAPHelper

+ (RageIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static RageIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"CanteceGradinita.ro.month",
                                      @"CanteceGradinita.ro.year",
                                      @"CanteceGradinita.ro.one",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
