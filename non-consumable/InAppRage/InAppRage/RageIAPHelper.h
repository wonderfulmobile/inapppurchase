//
//  RageIAPHelper.h
//  InAppRage
//
//  Created by User on 7/28/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"

@interface RageIAPHelper  : IAPHelper

+ (RageIAPHelper *)sharedInstance;

@end
