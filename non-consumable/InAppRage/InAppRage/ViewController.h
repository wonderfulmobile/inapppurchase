//
//  ViewController.h
//  InAppRage
//
//  Created by User on 7/28/15.
//  Copyright (c) 2015 steven. All rights reserved.
//

#import <UIKit/UIKit.h>

// 1
#import "RageIAPHelper.h"
#import <StoreKit/StoreKit.h>

@interface ViewController : UIViewController {
    NSArray *_products;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

